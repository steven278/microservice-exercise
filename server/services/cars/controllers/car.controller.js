const { Car } = require('../models');
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const getAllCars = async(req, res, next) => {
    try{
        const { name } = req.query;
        let { page, row } = req.query;
        if(!page || !row){
            page = 1;
            row = 5;
        }
        const options = {
            attributes : ['id', 'name', 'code', 'number_of_gears', 'number_of_tires', 'car_type'],
            limit : row,
            offset : (page - 1) * row,
        }
        if(name){
            options.where = { name : { [Op.like] : `%${name}%` } }
        }
        const data = await Car.findAll(options);
        
        res.status(200).json({
            status: 'success',
            data,
        })
    }catch(error){
        next(error);
    }
}

const getCarById = async(req, res, next) => {
    try{
        const { id } = req.params;
        const data = await Car.findByPk(id);
        if(!data){
            throw new Error(`Cannot get car with id ${id}`);
        }
        res.status(200).json({
            status : 'success',
            data : data
        })
    }catch(error){
        next(error);
    }
}

const createCar = async(req, res, next) => {
    try {
        const { name, code, number_of_gears, number_of_tires, car_type } = req.body;
        console.log(name, code, number_of_gears, number_of_tires, car_type);
        const data = await Car.create({
            name,
            code,
            number_of_gears,
            number_of_tires,
            car_type
        });
        if(!data){
            throw new Error(`Failed to create car data`);
        }
        res.status(200).json({
            status: 'success',
            createdData : data
        });
    } catch (error) {
        next(error);
    }
}

const updateCar = async(req, res, next) => {
    try {
        const { name, code, number_of_gears, number_of_tires, car_type } = req.body;
        const { id } = req.params;
        const data = await Car.update({
            name,
            code,
            number_of_gears,
            number_of_tires,
            car_type
        }, { where: { id }, plain: true, returning: true});
        res.status(200).json({
            status: 'success',
            updatedData : data[1]
        });
    } catch (error) {
        next(error)
    }
}

const deleteCar = async(req, res, next) => {
    try {
        const { id } = req.params;
        const data = await Car.destroy({
            where: { id },
            plain: true,
            returning: true
        });
        if(!data){
            throw new Error(`Failed to delete car with id ${id}`);
        }
        res.status(200).json({
            status : 'success',
            deletedData: `Successfully deleted car with id ${id}`
        })
    } catch (error) {
        next(error);
    }
}

module.exports = {
    getAllCars,
    getCarById,
    createCar,
    updateCar,
    deleteCar
}