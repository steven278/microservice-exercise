const router = require('express').Router();
const carRoutes = require('./car.routes');

router.get('/', (req, res) => {
    res.send('hello world')
});
router.use('/cars', carRoutes);

router.use((err, req, res, next) => {
    if(err.name === 'SequelizeDatabaseError' || err.name === 'SequelizeUniqueConstraintError' ||  err.name === 'ReferenceError'){
        res.status(400).json({
            status : 'bad request',
            errorName : err.name,
            message : err.message
        });
    }else if( err.name === 'Error' ){
        res.status(404).json({
            status : 'not found',
            errorName : err.name,
            message : err.message
        });
    }
    res.status(500).json({
        status : 'internal server error',
        errorName : err.name,
        message : err.message
    });
})
module.exports = router;