require('dotenv').config();
const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const routes = require('./routes/index.routes');

app.use(express.json());
app.use(express.urlencoded({ extended:true }));
app.use(`${process.env.BASE_URL}`, routes);

app.listen(port, () => {
    console.log(`cars is listening on port ${port}`);
})