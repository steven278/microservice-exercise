'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Cars', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        allowNull:false,
        type: Sequelize.STRING
      },
      code: {
        allowNull:false,
        unique:true,
        type: Sequelize.STRING
      },
      number_of_gears: {
        allowNull:false,
        type: Sequelize.INTEGER
      },
      number_of_tires: {
        allowNull:false,
        type: Sequelize.INTEGER
      },
      car_type: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: 'Types',
          },
          key: 'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Cars');
  }
};