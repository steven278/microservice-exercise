'use strict';

const carTypes = require('../masterdata/car-types.json');

module.exports = {
  async up (queryInterface, Sequelize) {
    const carTypesTimestamped = carTypes.map(type => {
      type.createdAt = new Date();
      type.updatedAt = new Date();
      return type;
    });
    await queryInterface.bulkInsert('Types', carTypesTimestamped, {});
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Types', null, { truncate: true, restartIdentity: true });
  }
};
