'use strict';

const cars = require('../masterdata/cars.json');

module.exports = {
  async up (queryInterface, Sequelize) {
    const carsTimestamped = cars.map(car => {
      car.createdAt = new Date();
      car.updatedAt = new Date();
      return car;
    });
    await queryInterface.bulkInsert('Cars', carsTimestamped, {});
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Cars', null, { truncate: true, restartIdentity: true });
  }
};
