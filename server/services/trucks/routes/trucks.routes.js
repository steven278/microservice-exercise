const router = require('express').Router();
const { getAllTrucks, getTruckById, createTruck, updateTruck, deleteTruck } = require('../controllers/truck.controller');

router.get('/', getAllTrucks);
router.get('/:id', getTruckById);
router.post('/', createTruck);
router.put('/:id', updateTruck);
router.delete('/:id', deleteTruck);

module.exports = router;