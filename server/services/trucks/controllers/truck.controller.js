const { Truck } = require('../models');
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const getAllTrucks = async(req, res, next) => {
    try{
        const { name } = req.query;
        let { page, row } = req.query;
        if(!page || !row){
            page = 1;
            row = 5;
        }
        const options = {
            attributes : ['id', 'name', 'code', 'number_of_gears', 'number_of_tires'],
            limit : row,
            offset : (page - 1) * row,
        }
        if(name){
            options.where = { name : { [Op.like] : `%${name}%` } }
        }
        const data = await Truck.findAll(options);
        
        res.status(200).json({
            status: 'success',
            data
        })
    }catch(error){
        next(error);
    }
}

const getTruckById = async(req, res, next) => {
    try{
        const { id } = req.params;
        const data = await Truck.findByPk(id);
        if(!data){
            throw new Error(`Cannot get truck with id ${id}`);
        }
        res.status(200).json({
            status : 'success',
            data : data
        })
    }catch(error){
        next(error);
    }
}

const createTruck = async(req, res, next) => {
    try {
        const { name, code, number_of_gears, number_of_tires} = req.body;
        const data = await Truck.create({
            name,
            code,
            number_of_gears,
            number_of_tires
        });
        if(!data){
            throw new Error(`Failed to create truck data`);
        }
        res.status(200).json({
            status: 'success',
            createdData : data
        });
    } catch (error) {
        next(error);
    }
}

const updateTruck = async(req, res, next) => {
    try {
        const { name, code, number_of_gears, number_of_tires } = req.body;
        const { id } = req.params;
        const data = await Truck.update({
            name,
            code,
            number_of_gears,
            number_of_tires
        }, { where: { id }, plain: true, returning: true});
        if(!data){
            throw new Error(`Failed to update truck data`);
        }
        res.status(200).json({
            status: 'success',
            updatedData : data[1]
        });
    } catch (error) {
        next(error);
    }
}

const deleteTruck = async(req, res, next) => {
    try {
        const { id } = req.params;
        const data = await Truck.destroy({
            where: { id },
            plain: true,
            returning: true
        });
        if(!data){
            throw new Error(`Failed to delete truck with id ${id}`);
        }
        res.status(200).json({
            status : 'success',
            deletedData: `Successfully deleted truck with id ${id}`
        })
    } catch (error) {
        next(error);
    }
}

module.exports = {
    getAllTrucks,
    getTruckById,
    createTruck,
    updateTruck,
    deleteTruck
}