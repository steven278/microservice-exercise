'use strict';

const trucks = require('../masterdata/trucks.json');
module.exports = {
  async up (queryInterface, Sequelize) {
    const trucksTimestamped = trucks.map(truck => {
      truck.createdAt = new Date();
      truck.updatedAt = new Date();
      return truck;
    });
    await queryInterface.bulkInsert('Trucks', trucksTimestamped, {});
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Trucks', null, { truncate: true, restartIdentity: true });
  }
};
