const router = require('express').Router();
const { getAllCars, createCar, deleteCar } = require('../controllers/car.controller');

router.get('/', getAllCars);
router.post('/', createCar);
router.delete('/:id', deleteCar);

module.exports = router;