const router = require('express').Router();
const { getAllTrucks, updateTruck, deleteTruck } = require('../controllers/truck.controller');

router.get('/', getAllTrucks);
router.put('/:id', updateTruck);
router.delete('/:id', deleteTruck);

module.exports = router;