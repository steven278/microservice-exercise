const router = require('express').Router();
const { getAllCarsAndTrucks } = require('../controllers/carAndTruck.controller');

router.get('/', getAllCarsAndTrucks);

module.exports = router;