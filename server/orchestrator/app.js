require('dotenv').config();

const express = require('express');
const app =  express();
const port = process.env.PORT || 4000;
const BASE_URL = process.env.BASE_URL;
const routes = require('./routes/index.routes');

app.use(express.json());
app.use(express.urlencoded( { extended: true } ));

app.use(`${BASE_URL}/feugeseot`, routes);

app.listen(port, () => {
    console.log(`Orchestrator is listening on port ${port}`)
});