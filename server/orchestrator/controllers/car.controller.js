const carsUrl = process.env.URL_CARS;
const axios = require('axios').default;

const getAllCars = async (req, res, next) => {
    try {
        let { page, row } = req.query;
        if(!page || !row){
            page = 1;
            row = 5;
        }
        const cars = await axios.get(`${carsUrl}`, { params: { page, row } }).then(data => data.data);
        res.status(200).json({
            status: 'success',
            cars,
        });
    } catch (error) {
        next(error);
    }
}

const createCar = async(req, res, next) => {
    try {
        const { name, code, number_of_gears, number_of_tires, car_type } = req.body;
        const data = await axios.post(`${carsUrl}`, {
            name,
            code,
            number_of_gears,
            number_of_tires,
            car_type
        }).then(data => data.data);
        res.status(200).json({
            status: 'success',
            createdData : data
        });
    } catch (error) {
        next(error);
    }
}

const deleteCar = async(req, res, next) => {
    try {
        const { id } = req.params;
        const data = await axios.delete(`${carsUrl}/${id}`).then(data => data.data);
        res.status(200).json({
            status: 'success',
            createdData : data
        });
    } catch (error) {
        next(error);
    }
}

module.exports = {
    getAllCars,
    createCar,
    deleteCar
}