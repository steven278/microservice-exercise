const trucksURL = process.env.URL_TRUCKS;
const axios = require('axios').default;

const getAllTrucks = async (req, res, next) => {
    try {
        let { page, row } = req.query;
        if(!page || !row){
            page = 1;
            row = 5;
        }
        const trucks = await axios.get(`${trucksURL}`, { params: { page, row } }).then(data => data.data);
        res.status(200).json({
            status: 'success',
            trucks,
        });
    } catch (error) {
        next(error);
    }
}

const updateTruck = async(req, res, next) => {
    try {
        const { id } = req.params;
        const { name, code, number_of_gears, number_of_tires } = req.body;
        const data = await axios.put(`${trucksURL}/${id}`, {
            name,
            code,
            number_of_gears,
            number_of_tires,
        }).then(data => data.data);
        res.status(200).json({
            status: 'success',
            createdData : data
        });
    } catch (error) {
        next(error);
    }
}

const deleteTruck = async(req, res, next) => {
    try {
        const { id } = req.params;
        const data = await axios.delete(`${trucksURL}/${id}`).then(data => data.data);
        res.status(200).json({
            status: 'success',
            createdData : data
        });
    } catch (error) {
        next(error);
    }
}


module.exports = {
    getAllTrucks,
    updateTruck,
    deleteTruck
}