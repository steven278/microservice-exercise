const carsUrl = process.env.URL_CARS;
const trucksURL = process.env.URL_TRUCKS;
const axios = require('axios').default;

const getAllCarsAndTrucks = async (req, res, next) => {
    try {
        const { name } = req.query;
        let { page, row } = req.query;
        if(!page || !row){
            page = 1;
            row = 5;
        }
        const cars = await axios.get(`${carsUrl}`, { params: { name, page, row } }).then(data => data.data);
        const trucks = await axios.get(`${trucksURL}`, { params: { name ,page, row } }).then(data => data.data);
        res.status(200).json({
            status: 'success',
            cars,
            trucks
        });
    } catch (error) {
        next(error);
    }
}

module.exports = {
    getAllCarsAndTrucks
}